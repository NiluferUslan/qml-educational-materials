**Topic**: Quantum Machine Learning

**Contributors**: Ayşin Taşdelen, Cheshta Joshi, Rabins Wosti, Viktorija Leimane

**Requirements**

 - basics of quantum computing
 - basic level python
 
 **Description**: The aim of this project is to prepare quantum machine learning teaching material for QPool2019 project. 
